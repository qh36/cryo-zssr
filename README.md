# Cryo-ZSSR

This is the repository for Cryo-ZSSR: Multiple Image Super Resolution Using Deep Internal Learning: [Link to paper](https://arxiv.org/pdf/2011.11020.pdf)
## Environment

To run the code, please first create a conda environment `environment.yml` which should include all the necessary packages/add-ons. 

```bash
conda env create -f environment.yml
```
Once the environment is created, activate it by calling:
```bash
conda activate pytorch
```

Please make sure you are using **python3** while running the code. 

## Usage
Make sure your GPU is enabled to run the code. 
### General Usage on data:
```bash
python run_ZSSR.py <config1> <GPU option> <config2> 
```
`<config1>`: is an instance of configs.Config class. Please see `config.py` file to adjust certain configurations for training (epochs, learning rate, crop size etc.) The config that is used to generate results provided in the paper is given with name `X2_REAL_CONF`. To use this config, replace `<config1>` with  `X2_REAL_CONF`. 


`<GPU option>`: Run on a specific GPU: replace `<GPU option>` with `0` or other specified GPU. 

`<config2>`: The `config.json` file to adjust parameters for network (number of kernels, kernel size, etc). To use the config, replace `<config2>` with `path/to/config.json`. The given config.json file has parameters that is used to generate results provided in the paper. A sample `config.json` file is in configs directory.
### Usage example:
```bash
python run_ZSSR.py X2_REAL_CONF 0 configs/config.json
```
### Data
The input data needs to be multi-frame mrc files. Specifically, number of frames needs to be a power of two. As noted in paper, multi-frames are **NOT** raw frames from movie stacks. Instead, they are multiple frame averages each aligned to a different reference frame. These frame averages can be generated from raw movie stacks using **MotionCorr2**. Raw dataset can be downloaded from **EMPIAR** database. In paper, we used [EMPIAR 10146](https://www.ebi.ac.uk/pdbe/emdb/empiar/entry/10146/). To align frames to a different reference frame using **MotionCorr2**, do something like the following:
```bash
/path/to/motioncorr2 -InMrc <input_movie> -OutMrc <output_average_name> -Patch 2 2 -Group 3 -FmRef <reference_frame_number> 
```
If you are using a total of 16 frames, you will need to align the movie 16 times, each time a different reference frame. Once all frame averages are generated, use `newstack` from `IMOD` (or other similar softwares) to combine all frames into a single .mrc file. In our paper, the inputs are 2X down-sampled frame averages from the original files. To downsample, we used the following command. 
```bash
newstack -input $i -output ${i%.*}_binned.${i##*.} -bin 2 -antialias 6
```
 Currently, the data folder is set to single_data. All data we used in paper are provided in Data folder.  To use the data inside that folder, make sure to update .input_path to the correct data path in `config.py`. To use custom dataset, make sure all .mrc files are in a single folder.

Currently, the code only takes in input size of *num_of_frame*s x 512x512. *If the provided size is bigger than that, it will automatically crop it to 512x512 (Due to memory limitation)*. The output will be 2X up-sampled of size 1024x1024. 

### Output
Output will be stored in `results_path` from `config.py` file. It will create a folder with name format *test_Date_time*. The output will include 1 .png SR image for visualization and 1 .mrc file. 
Sample output is provided in `results` folder. 

### Hardware 
We used NVIDIA TITAN V with 32 GB memory. A GPU with smaller than 32GB memory will likely encounter memory issues. 

## Contributing
Please feel free to contact me if you run into any issues: qh36@duke.edu

## Special Note:
Part of the code is based on ZSSR code from [ZSSR](https://github.com/assafshocher/ZSSR)
and HighResNet from [HighResNet](https://github.com/ElementAI/HighRes-net)

