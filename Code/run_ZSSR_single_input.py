import sys
import os
import config
import train_hrnet

"""
Code based on ZSSR provided by Assaf Shocher
https://github.com/assafshocher/ZSSR

"""

def main(input_img, ground_truth, kernels, gpu, conf_str, config_str,results_path):
    # Choose the wanted GPU
    if gpu is not None:
        os.environ["CUDA_VISIBLE_DEVICES"] = '%s' % gpu

    # 0 input for ground-truth or kernels means None
    ground_truth = None if ground_truth == '0' else ground_truth
    print ('*****', kernels)
    kernels = None if kernels == '0' else kernels.split(';')[:-1]

    # Setup configuration and results directory
    conf = config.Config()
    if conf_str is not None:
        exec ('conf = config.%s' % conf_str)
    conf.result_path = results_path

    # Run ZSSR on the image
    net = train_hrnet.ZSSR_multi_2(input_img, input_img, conf, config_str, ground_truth, kernels)
    net.run()


if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6], sys.argv[7])
