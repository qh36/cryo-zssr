import torch.nn as nn
import torch
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.gridspec import GridSpec
#from config import Config 

from utils import *
from model import simpleNet
from loss import GeneratorLoss
from loss import GeneratorLoss3
from loss import NormalLoss
from loss import L1_loss
import mrcfile
import os 
from torch.utils.tensorboard import SummaryWriter
#from model import FRVSR
import cv2
from scipy.io import loadmat
import json
from model import FRVSR
from model import ShiftNet
from model import HRNet

"""
Code based on ZSSR provided by Assaf Shocher
https://github.com/assafshocher/ZSSR

"""



class ZSSR_multi_2:
	torch.cuda.set_device(0)
	kernel = None
	learning_rate = None
	hr_father = None
	lr_son = None
	avg = None
	avg_father = None
	#diff_hr_father = None
	sr = None
	sf = None
	gt_per_sf = None
	final_sr = None
	hr_fathers_sources = []
	name_tb = 'zssr_model'
	save_tensorboard_dir = f'tb-{name_tb}'
	# Output variables initialization / declaration
	reconstruct_output = None
	train_output = None
	output_shape = None

	# Counters and logs initialization
	iter_ = 0
	base_sf = 1.0
	base_ind = 0
	sf_ind = 0
	mse = []
	mse_rec = []
	interp_rec_mse = []
	interp_mse = []
	mse_steps = []
	loss = []
	learning_rate_change_iter_nums = []
	fig = None

	# Network tensors (all tensors end with _t to distinguish)
	learning_rate_t = None
	lr_son_t = None
	hr_father_t = None
	filters_t = None
	layers_t = None
	net_output_t = None
	loss_t = None
	train_op = None
	init_op = None

	# Parameters related to plotting and graphics
	plots = None
	loss_plot_space = None
	lr_son_image_space = None
	hr_father_image_space = None
	out_image_space = None

	def __init__(self, input_img, avg_img, conf, config, ground_truth = None, kernels= None):
		torch.backends.cudnn.enabled = False 
		self.conf = conf
		self.cuda = conf.cuda
		self.device = conf.device
		#self.input = input_img 
		if type(input_img) is not str:
			self.input = input_img
		else:
			with mrcfile.open(input_img, permissive=True) as mrc:
				self.input = mrc.data
		self.input = self.input[:,:512,:512]
		if type(config) is not str:
			self.config = config 
		else:
			with open(config,"r") as read_file:
				self.config = json.load(read_file)

		num_of_imgs = self.input.shape[0] 

		normalized_input = np.zeros(self.input.shape)
		for i in range(num_of_imgs):

			normalized_input[i,:,:] = cv2.normalize(self.input[i,:,:],None,alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)

		self.input = normalized_input
		num_of_frames = self.input.shape[0]
		assert (num_of_frames % 2== 0),"number of input frames needs to be power of 2"
		self.avg = normalized_input[0,:,:]
		#self.avg = cv2.normalize(self.avg, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
		self.Y = True
		self.gt = ground_truth if type(ground_truth) is not str else img.imread(ground_truth)
		self.kernels = preprocess_kernels(kernels, conf)
		self.model = HRNet(self.config["network"])
		self.regis_model = ShiftNet()
		if self.conf.init_net_for_each_sf == True:
			self.model = HRNet(self.config["network"])
		if self.cuda:
			self.model = self.model.to(self.device)
			self.regis_model = self.regis_model.to(self.device)
		self.writer = SummaryWriter(self.save_tensorboard_dir)
		print(self.model)
		num_params = sum(p.numel() for p in self.model.parameters() if p.requires_grad)
		print('trainable parameters:', num_params)
		self.init_parameters()

		# The first hr father source is the input (source goes through augmentation to become a father)
		self.hr_fathers_sources = [self.input]

		# We keep the input file name to save the output with a similar name. If array was given rather than path
		# then we use default provided by the configs
		#self.file_name = input_img if type(input_img) is str else conf.name
		self.file_name = input_img if type(input_img) is str else conf.name 

	def run(self):
		if not os.path.exists(self.save_tensorboard_dir):
			os.mkdir(self.save_tensorboard_dir)
		for self.sf_ind, (sf, self.kernel) in enumerate(zip(self.conf.scale_factors, self.kernels)):
			print('** Start training for sf=', sf, ' **')
			if np.isscalar(sf):
				sf = [sf, sf]
			self.sf = np.array(sf) / np.array(self.base_sf)
			self.output_shape = np.uint(np.ceil(np.array(self.avg.shape[0:2])*sf))
			#print('input shape', self.input.shape)
			self.init_parameters()
			self.train()
			post_processed_output = self.final_test()
			self.hr_fathers_sources.append(post_processed_output)
			self.base_change()
			# print('post process shape')
			# print(post_processed_output.shape)
			if self.conf.save_results:
				sf_str = ''.join('X%.2f' % s for s in self.conf.scale_factors[self.sf_ind])
				plt.imsave('%s/%s_zssr_%s.png' %
						   (self.conf.result_path, os.path.basename(self.file_name)[:-4], sf_str),
						   post_processed_output, vmin=0, vmax=1, cmap='gray')
				with mrcfile.new('%s/%s_zssr_%s.mrc' %
						   (self.conf.result_path, os.path.basename(self.file_name)[:-4], sf_str)) as mrc:
					mrc.set_data(post_processed_output)

			print('** Done training for sf=', sf, ' **')

		return post_processed_output



	def init_parameters(self):
		self.loss = [None]*self.conf.max_iters
		self.mse, self.mse_rec, self.interp_mse, self.interp_rec_mse, self.mse_steps = [], [], [], [], []
		self.iter = 0
		self.learning_rate = self.conf.learning_rate
		self.learning_rate_change_iter_nums = [0]
		self.gt_per_sf = (imresize(self.gt,
								   scale_factor=self.sf / self.conf.scale_factors[-1],
								   output_shape=self.output_shape,
								   kernel=self.conf.downscale_gt_method)
						  if (self.gt is not None and
							  self.sf is not None and
							  np.any(np.abs(self.sf - self.conf.scale_factors[-1]) > 0.01))
						  else self.gt)

	
	def register_batch(self,shiftNet, lrs, reference):
		"""
		Registers images against references.
		Args:
			shiftNet: torch.model
			lrs: tensor (batch size, views, W, H), images to shift
			reference: tensor (batch size, W, H), reference images to shift
		Returns:
			thetas: tensor (batch size, views, 2)
		"""
	
		n_views = lrs.size(1)
		thetas = []
		for i in range(n_views):
			theta = shiftNet(torch.cat([reference, lrs[:, i : i + 1]], 1))
			thetas.append(theta)
		thetas = torch.stack(thetas, 1)

		return thetas

	def apply_shifts(self,shiftNet, images, thetas, device):
		"""
		Applies sub-pixel translations to images with Lanczos interpolation.
		Args:
			shiftNet: torch.model
			images: tensor (batch size, views, W, H), images to shift
			thetas: tensor (batch size, views, 2), translation params
		Returns:
			new_images: tensor (batch size, views, W, H), warped images
		"""
	
		batch_size, n_views, height, width = images.shape
		images = images.view(-1, 1, height, width)
		thetas = thetas.view(-1, 2)
		new_images = shiftNet.transform(thetas, images, device=device)

		return new_images.view(-1, n_views, images.size(2), images.size(3))
	def forward_backward_pass(self, lr_son, avg_son, hr_father, criterion, optimizer, loss, alpha):

		interpolated_lr_son = np.zeros((lr_son.shape[0],lr_son.shape[1]*2, lr_son.shape[2]*2))
		for i in range(lr_son.shape[0]):
			interpolated_lr_son[i,:,:] = imresize(lr_son[i,:,:], self.sf, (lr_son.shape[1]*2, lr_son.shape[2]*2), self.conf.upscale_method)

		if self.Y == True:
			lr_son_input = torch.Tensor(interpolated_lr_son).unsqueeze_(0)

			alpha = alpha.unsqueeze_(0)
		else:
			lr_son_input = torch.Tensor(interpolated_lr_son).permute(2,0,1).unsqueeze_(0)
			alpha = alpha.unsqueeze_(0)
		lr_son_input = lr_son_input.requires_grad_()

		if self.cuda == True:
			alpha = alpha.to(self.device)
			lr_son_input = lr_son_input.to(self.device)
		train_output = self.model(lr_son_input,alpha)
		father = hr_father.view(-1,1,256,256)
		shifts = self.register_batch(self.regis_model,train_output,reference = father)
		srs_shifted = self.apply_shifts(self.regis_model,train_output, shifts,self.device)[:,0]

		#print(srs_shifted.shape)
		srs_shifted = srs_shifted.unsqueeze_(0)
		loss = criterion(srs_shifted, hr_father)
		# if last_frame:
		optimizer.zero_grad()
		loss.backward()
		optimizer.step()
		self.loss[self.iter] = loss
		torch.cuda.empty_cache()
		return np.clip(np.squeeze(srs_shifted.cpu().detach().numpy()),0,1)

	def forward_pass(self, lr_son, alpha, hr_father_shape = None):
		interpolated_lr_son = np.zeros((lr_son.shape[0],lr_son.shape[1]*2, lr_son.shape[2]*2))
		for i in range(lr_son.shape[0]):
			interpolated_lr_son[i,:,:] = imresize(lr_son[i,:,:], self.sf, (lr_son.shape[1]*2, lr_son.shape[2]*2), self.conf.upscale_method)
		#print(interpolated_lr_son.shape)
		if self.Y == True:
			lr_son_input = torch.Tensor(interpolated_lr_son).unsqueeze_(0)
			alpha = alpha.unsqueeze_(0)
		else:
			alpha = alpha.unsqueeze_(0)
			lr_son_input = torch.Tensor(interpolated_lr_son).permute(2,0,1).unsqueeze_(0)
		if self.cuda:
			lr_son_input = lr_son_input.to(self.device)
			alpha = alpha.to(self.device)
		
		with torch.no_grad():
			self.model.eval()
			output = self.model(lr_son_input,alpha)
			return np.clip(np.squeeze(output.cpu().detach().permute(0,2,3,1).numpy()),0,1)

		
	def learning_rate_policy(self):
		# fit linear curve and check slope to determine whether to do nothing, reduce learning rate or finish
		if (not (1 + self.iter) % self.conf.learning_rate_policy_check_every
				and self.iter - self.learning_rate_change_iter_nums[-1] > self.conf.min_iters):
			# noinspection PyTupleAssignmentBalance
			#print(self.conf.run_test_every)
			[slope, _], [[var, _], _] = np.polyfit(self.mse_steps[-(self.conf.learning_rate_slope_range //
																	self.conf.run_test_every):],
												   self.mse_rec[-(self.conf.learning_rate_slope_range //
																  self.conf.run_test_every):],
												   1, cov=True)

			# We take the the standard deviation as a measure
			std = np.sqrt(var)

			# Verbose
			print('slope: ', slope, 'STD: ', std)

			# Determine learning rate maintaining or reduction by the ration between slope and noise
			if -self.conf.learning_rate_change_ratio * slope < std:
				self.learning_rate /= 10
				print("learning rate updated: ", self.learning_rate)

				# Keep track of learning rate changes for plotting purposes
				self.learning_rate_change_iter_nums.append(self.iter)

	def quick_test(self):
		torch.cuda.empty_cache()
		# There are four evaluations needed to be calculated:

		# 1. True MSE (only if ground-truth was given), note: this error is before post-processing.
		# Run net on the input to get the output super-resolution (almost final result, only post-processing needed)
		
		self.alpha = torch.ones(self.input.shape[0])
		#for i in range(self.input.shape[0]):
		self.sr = self.forward_pass(self.input,self.alpha)
		#self.sr = self.forward_pass(self.input_single)
		self.mse = (self.mse + [np.mean(np.ndarray.flatten(np.square(self.gt_per_sf - self.sr)))]
					if self.gt_per_sf is not None else None)

		# 2. Reconstruction MSE, run for reconstruction- try to reconstruct the input from a downscaled version of it
		# avg_son = self.father_to_son(self.avg)
		# avg_son_interp = imresize(avg_son, self.sf, self.avg.shape, self.conf.upscale_method)
		# if self.Y == True:
		#     avg_son = torch.Tensor(avg_son).unsqueeze_(0).unsqueeze_(0)
		#     avg_son_interp = torch.Tensor(avg_son_interp).unsqueeze_(0).unsqueeze_(0)
		# if self.cuda:
		#     avg_son = avg_son.to(self.device)
		#     avg_son_interp = avg_son_interp.to(self.device)
		#self.model.init_hidden(self.device, avg_son, avg_son_interp)
		del self.sr
		torch.cuda.empty_cache()
		all_lr_son = []
		for i in range(self.input.shape[0]):
			all_lr_son.append(self.father_to_son(self.input[i,:,:]))
		all_lr_son = np.asarray(all_lr_son)


		self.reconstruct_output = self.forward_pass(all_lr_son, self.alpha)

		#self.reconstruct_output = self.forward_pass(self.father_to_son(self.input_single), self.input_single.shape)
		self.mse_rec.append(np.mean(np.ndarray.flatten(np.square(self.avg - self.reconstruct_output))))

		# 3. True MSE of simple interpolation for reference (only if ground-truth was given)
		interp_sr = imresize(self.avg, self.sf, self.output_shape, self.conf.upscale_method)
		self.interp_mse = (self.interp_mse + [np.mean(np.ndarray.flatten(np.square(self.gt_per_sf - interp_sr)))]
						   if self.gt_per_sf is not None else None)

		# 4. Reconstruction MSE of simple interpolation over downscaled input
		interp_rec = imresize(self.father_to_son(self.avg), self.sf, self.avg.shape[0:2], self.conf.upscale_method)
		self.interp_rec_mse.append(np.mean(np.ndarray.flatten(np.square(self.avg - interp_rec))))

		# Track the iters in which tests are made for the graphics x axis
		self.mse_steps.append(self.iter)

		# Display test results if indicated
		if self.conf.display_test_results:
			print('iteration: ', self.iter, 'reconstruct mse:', self.mse_rec[-1], ', true mse:', (self.mse[-1]
																								  if self.mse else None))

		# plot losses if needed
		if self.conf.plot_losses:
			self.plot()

	def train(self):


		criterion = GeneratorLoss3(self.device)
		#criterion = NormalLoss(self.trained_model, self.device)
		#criterion = L1_loss(self.trained_model, self.device)
		optimizer = torch.optim.Adam(self.model.parameters(),lr = self.learning_rate)
		for self.iter in range(self.conf.max_iters):

			self.hr_father,self.avg_father= random_augment_all(ims=self.hr_fathers_sources,avg = self.avg,
											base_scales=[1.0] + self.conf.scale_factors,
											leave_as_is_probability=self.conf.augment_leave_as_is_probability,
											no_interpolate_probability=self.conf.augment_no_interpolate_probability,
											min_scale=self.conf.augment_min_scale,
											max_scale=([1.0] + self.conf.scale_factors)[len(self.hr_fathers_sources)-1],
											allow_rotation=self.conf.augment_allow_rotation,
											scale_diff_sigma=self.conf.augment_scale_diff_sigma,
											shear_sigma=self.conf.augment_shear_sigma,
											crop_size=self.conf.crop_size)
			all_lr_son = []
			for i in range(self.hr_father.shape[0]):
				single_son = self.father_to_son(self.hr_father[i,:,:])
				all_lr_son.append(single_son)
			self.lr_son = all_lr_son
			self.lr_son = np.asarray(self.lr_son)
			self.avg_son = self.father_to_son(self.avg_father)

			self.avg_son_interp = imresize(self.avg_son, self.sf, self.avg_father.shape, self.conf.upscale_method)

			# e
			if self.Y == True:
				self.avg_son = torch.Tensor(self.avg_son).unsqueeze_(0).unsqueeze_(0)
				self.avg_father = torch.Tensor(self.avg_father).unsqueeze_(0).unsqueeze_(0)
				#self.avg_son_interp = torch.Tensor(self.avg_son_interp).unsqueeze_(0).unsqueeze_(0)
			if self.cuda:
				self.avg_son = self.avg_son.to(self.device)
				self.avg_father = self.avg_father.to(self.device)

			num_of_frames = len(self.lr_son)

			last_frame = False
			loss = 0 
			self.alphas = torch.ones(num_of_frames)
			self.train_output = self.forward_backward_pass(self.lr_son, self.avg_son, self.avg_father,criterion,optimizer,loss,self.alphas)
			torch.cuda.empty_cache()


			#self.train_output = self.forward_backward_pass(self.lr_son, self.hr_father, criterion, optimizer)
			if not self.iter % self.conf.display_every:
				print('sf:', self.sf*self.base_sf, ', iteration: ', self.iter, ', loss: ', self.loss[self.iter])
			self.writer.add_scalar('training loss', self.loss[self.iter].item(), self.iter)
			self.writer.add_scalar('learning rate', self.learning_rate, self.iter)
			# Test network
			if self.conf.run_test and (not self.iter % self.conf.run_test_every):
				self.quick_test()

			# Consider changing learning rate or stop according to iteration number and losses slope
			self.learning_rate_policy()

			# stop when minimum learning rate was passed
			if self.learning_rate < self.conf.min_learning_rate:
				break
			# if self.loss[self.iter].item() < 0.0001:
			# 	break 
	def father_to_son(self, hr_father):
		# Create son out of the father by downscaling and if indicated adding noise
		lr_son = imresize(hr_father, 1.0 / self.sf, kernel=self.kernel)

		return np.clip(lr_son + np.random.randn(*lr_son.shape) * self.conf.noise_std, 0, 1)

	def final_test(self):
		# Run over 8 augmentations of input - 4 rotations and mirror (geometric self ensemble)
		outputs = []

		# The weird range means we only do it once if output_flip is disabled
		# We need to check if scale factor is symmetric to all dimensions, if not we will do 180 jumps rather than 90
		for k in range(0, 1 + 7 * self.conf.output_flip, 1 + int(self.sf[0] != self.sf[1])):
			avg_son = np.rot90(self.avg, k) if k < 4 else np.fliplr(np.rot90(self.avg, k))
			avg_son = cv2.normalize(avg_son, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
			avg_father = imresize(avg_son, self.sf, None, self.conf.upscale_method)
			if self.Y == True:
				avg_son = torch.Tensor(avg_son).unsqueeze_(0).unsqueeze_(0)
				avg_father = torch.Tensor(avg_father).unsqueeze_(0).unsqueeze_(0)
			if self.cuda:
				avg_son = avg_son.to(self.device)
				avg_father = avg_father.to(self.device)
			#self.model.init_hidden(self.device, avg_son, avg_father)
			test_input_all=[]
			for fr in range(self.input.shape[0]):

			# Rotate 90*k degrees and mirror flip when k>=4
				test_input = np.rot90(self.input[fr], k) if k < 4 else np.fliplr(np.rot90(self.input[fr], k))

				test_input = np.uint8(cv2.normalize(test_input, None, 0, 255, cv2.NORM_MINMAX))
				test_input= cv2.equalizeHist(test_input)
				test_input = cv2.normalize(test_input, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
				test_input_all.append(test_input)
			# Apply network on the rotated input
			test_input_all = np.asarray(test_input_all)
			test_alpha = torch.ones(self.input.shape[0])
			tmp_output = self.forward_pass(test_input_all, test_alpha)

			# Undo the rotation for the processed output (mind the opposite order of the flip and the rotation)
			tmp_output = np.rot90(tmp_output, -k) if k < 4 else np.rot90(np.fliplr(tmp_output), -k)

			# fix SR output with back projection technique for each augmentation
			# for bp_iter in range(self.conf.back_projection_iters[self.sf_ind]):
			# 	tmp_output = back_projection(tmp_output, self.avg, down_kernel=self.kernel,
			# 								 up_kernel=self.conf.upscale_method, sf=self.sf)

			# save outputs from all augmentations
			outputs.append(tmp_output)

		# Take the median over all 8 outputs
		almost_final_sr = np.median(outputs, 0)

		# Again back projection for the final fused result
		for bp_iter in range(self.conf.back_projection_iters[self.sf_ind]):
			almost_final_sr = back_projection(almost_final_sr, self.avg, down_kernel=self.kernel,
											  up_kernel=self.conf.upscale_method, sf=self.sf)

		# Now we can keep the final result (in grayscale case, colors still need to be added, but we don't care
		# because it is done before saving and for every other purpose we use this result)
		self.final_sr = almost_final_sr
		# self.final_sr = self.forward_pass(self.input,self.alpha)



		# Add colors to result image in case net was activated only on grayscale
		return self.final_sr

	def base_change(self):
		# If there is no base scale large than the current one get out of here
		if len(self.conf.base_change_sfs) < self.base_ind + 1:
			return

		# Change base input image if required (this means current output becomes the new input)
		if abs(self.conf.scale_factors[self.sf_ind] - self.conf.base_change_sfs[self.base_ind]) < 0.001:
			if len(self.conf.base_change_sfs) > self.base_ind:

				# The new input is the current output
				self.input_single = self.final_sr

				# The new base scale_factor
				self.base_sf = self.conf.base_change_sfs[self.base_ind]

				# Keeping track- this is the index inside the base scales list (provided in the config)
				self.base_ind += 1

			print('base changed to %.2f' % self.base_sf)

	def plot(self):
		plots_data, labels = zip(*[(np.array(x), l) for (x, l)
								   in zip([self.mse, self.mse_rec, self.interp_mse, self.interp_rec_mse],
										  ['True MSE', 'Reconstruct MSE', 'Bicubic to ground truth MSE',
										   'Bicubic to reconstruct MSE']) if x is not None])

		# For the first iteration create the figure
		if not self.iter:
			# Create figure and split it using GridSpec. Name each region as needed
			self.fig = plt.figure(figsize=(9.5, 9))
			grid = GridSpec(4, 4)
			self.loss_plot_space = plt.subplot(grid[:-1, :])
			self.lr_son_image_space = plt.subplot(grid[3, 0])
			self.hr_father_image_space = plt.subplot(grid[3, 3])
			self.out_image_space = plt.subplot(grid[3, 1])

			# Activate interactive mode for live plot updating
			plt.ion()

			# Set some parameters for the plots
			self.loss_plot_space.set_xlabel('step')
			self.loss_plot_space.set_ylabel('MSE')
			self.loss_plot_space.grid(True)
			self.loss_plot_space.set_yscale('log')
			self.loss_plot_space.legend()
			self.plots = [None] * 4

			# loop over all needed plot types. if some data is none than skip, if some data is one value tile it
			self.plots = self.loss_plot_space.plot(*[[0]] * 2 * len(plots_data))

		# Update plots
		for plot, plot_data in zip(self.plots, plots_data):
			plot.set_data(self.mse_steps, plot_data)

			self.loss_plot_space.set_xlim([0, self.iter + 1])
			all_losses = np.array(plots_data)
			self.loss_plot_space.set_ylim([np.min(all_losses)*0.9, np.max(all_losses)*1.1])

		# Mark learning rate changes
		for iter_num in self.learning_rate_change_iter_nums:
			self.loss_plot_space.axvline(iter_num)

		# Add legend to graphics
		self.loss_plot_space.legend(labels)

		# Show current input and output images
		self.lr_son_image_space.imshow(self.lr_son, vmin=0.0, vmax=1.0)
		self.out_image_space.imshow(self.train_output, vmin=0.0, vmax=1.0)
		self.hr_father_image_space.imshow(self.hr_father, vmin=0.0, vmax=1.0)

		# These line are needed in order to see the graphics at real time
		self.fig.canvas.draw()
		plt.pause(0.01)





		
